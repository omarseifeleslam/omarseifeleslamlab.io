---
title: About me
weight: -30
---
<img style="float: left; padding-right:10px" src="/media/omarseif.jpeg" alt="Omar Seif <" width="200"/>

I'm Omar Seif El Eslam a simple human being seeking to learn more as I found the more you 
know the more you discover that you don't really know.


I was born at July 1993, while growing up my love to getting to know what's inside 
everything has increased, finally to find myself graduated at Jan. 2018 with a BSc 
degree in Mechatronics Engineering.

After graduation I have worked for almost 2 years as a robotics Engineer, mainly I 
was working in an R&D section where I had to reverse some projects and work from scratch 
on other, I got my Mechanical design and circuit design skills increased in this job.

Currently I work as a Technical Supervisor in Fab Lab Egypt which is the best work 
enviroment you can work in.

I don't want to talk alot but one more thing I'd like to tell you about, my dream, 
I want to have a workshop where I can be surrounded with machines and have the ability 
to work on whatever project I like.

## Why do I want to join Fab Academy ?
My main reasons for joining Fab Academy is:

**First** It's an oportunity for me to learn more about fields I didn't encounter yet
and to increase the knowledge I already have.

**Second** It will be a simulation of my dream to get in touch with different 
technologies and machines, also it will be a great enhancement for my career.



