---
title: 4- Programming & Interface
weight: -30
---

Okay, Lets start programming, We need the Atml Ice programmer and FTDI cable.
We should connect the programmer with the right wiring, in my case it was plugged with the notch 
to the right, also insure that the FTDI is plugged correctly from your board design.

<img Style="left;" src="/media/T2-46.jpeg">

The first time you connect the programmer to your PC, it will start installing the Atml Ice driver,
open the device manager and ensure that the device has been installed correctly, if not download it
and install in manually.
download the folder then select update driver and choose I have the driver on my computer and select
the folder you have downloaded.

you can downlaod the drive from this [site](https://www.drvhub.net/devices/other-devices/atmel/ice-data-gateway) and click on the downlaod driver option.

{{< columns >}}
<img Style="left;" src="/media/T2-34.PNG">
<--->
<img Style="right;" src="/media/T2-52.jfif">
{{< /columns >}}

After installing the atmel ice driver open the arduino code "demo", Arduino IDE naturally doesn’t support the microcontroller we are using in the board, so you
should add support for our microcontroller ATtiny45. The first step is to add a link to the support
package by going to file->preferences->Additional Boards Manager URLs then paste the following link :
https://raw.githubusercontent.com/damellis/attiny/ide-1.6.x-boards-manager/package_damellis_attiny_index.json

Then install the support package by going to tools->Boards->Boards Manager
then search for attiny and click install.

Then go to Tools and set up the settings:

- Board: ATtiny24/44/84
- Processor: ATtiny44
- Clock: Internal 8 MHZ
- Programmer: Atmel-ICE (AVR)

*No need to select a port

<img Style="left;" src="/media/T2-29.png">

once you have your setup Connect both programmer and FTDI like the first photo in this page and 
click Sketch >> upload using programmer.

The programmer middle led is Red and it indicates that there's power connected so it should light up
once you connect it to the PC, other 2 leds are green one will be always light and this indicates
that the programmer driver and setup is correct and the last one lights only while uploading and it
indicates that everything is going well and the uploading is in progress.

<img Style="left;" src="/media/T2-45.jpeg">

Once uplaoded the leds should blink randomly and that's the dance code you have uploaded.

If everything is going well then go to the next step for the proccessor, if not go to challenges page.

Now we will start with Processing software, so lets ask first **what is processing:**
Processing is a flexible software sketchbook and a language for learning 
how to code within the context of the visual arts. Which means we will use processing with arduino 
to create a basic GUI [graphical user interface] to control the arduino code we have made.

Before running processing code we will do an edit at the end of the arduino code, Uncomment and 
comment the proper lines as indicated in the code then upload the code to the microcontroller.

Then remove the programmer and keep the FTDI, go to processing and run the code, 
it may run with no errors but an error about serial port may appear so what is this.

you will find a line in processing code that chooses the serial port 

`String myPort = Serial.list()[0];`

The Serial.list command is listing all your serial ports connected, for me the FTDI was the only
one connected and I knew that from device manager, you may go to your device manager and change
the number 0 to 1 or 2 or whatever the number of your serial port.

{{< columns >}}
<img Style="left;" src="/media/T2-32.PNG">
<--->
<img Style="right;" src="/media/T2-33.PNG">
{{< /columns >}}

once you have set your serial port correctly you can run your processing code and start playing.

Click on the photo below it will redirect you to a video.


[![Charleprlexing](/media/T2-50.jpeg)](https://drive.google.com/file/d/1NoGsCyj5Ht5GJkVaFAW0wyIy42lvmWOl/view?usp=sharing)