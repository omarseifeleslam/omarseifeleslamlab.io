---
title: 3- Soldering
weight: -30
---
We are having a board ready to solder now, we will get the list of components from the schematic 
and layout of the board.


Then we will prepare everything we need for soldering (Soldering iron, flux, sponge, solder wire & gift)

I used double-face tape to fix the board to the green matt to make it easier to solder, and started
soldering by applying solder tin to one leg of each component as we are soldering SMD components so
we are doing this to fix the component then solder the rest of legs after that.

<img Style="left;" src="/media/T2-43.jpeg">

Then start from the middle then the outer component, also start with the lowest hieght component
then the taller and so on.

The Soldering tecnique should be that you apply heat to the copper pad for like 2-3 seconds then
feed a small amount of the solder wire.

{{< hint info >}}
**Hint**\
Don't apply heat to the component for a long time that may break the component from inside.
{{< /hint >}}

<img Style="left;" src="/media/T2-4.jpeg">

After  finishing the soldering test each connection according to board layout and also test that
there's no short circuit connection in your PCB.

Then lets go to programming.