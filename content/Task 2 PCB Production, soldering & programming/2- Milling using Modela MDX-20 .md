---
title: 2- Milling using Modela MDX-20  
weight: -30
---
Once we have the png files ready we will go to our machine to start working on it.

First lets power up the machine and establish a connection from the pc. open a terminal and write 
down the command to open the file and start the npm.

Then open a browser with the ip in connection.

{{< columns >}}
<img Style="left;" src="/media/T2-20.png">
<--->
<img Style="right;" src="/media/T2-22.png">
{{< /columns >}}

This will open fabmodules we will select input format and choose image (.png) then output format 
Roland mill (.rml) then process PCB traces (1/64) and this is for the first image which is for 
traces.

{{< columns >}}
<img Style="left;" src="/media/T2-23.png">
<--->
<img Style="right;" src="/media/T2-24.png">
{{< /columns >}}

### Setting up the PCB and machine 

We will get our PCB and put on its back a layer of paint tape then above it a layer of double face
for fixing on the machine bed, and why not double face directly because it's highly adhesive with 
the PCB material so the paint tape for easy remove later after finishing.

After fixing the PCB on the machine bed we will get the first bit which is 0.4 V-bit to do the 
tracing with it 

{{< columns >}}
<img Style="left;" src="/media/T2-36.jpeg">
<--->
<img Style="right;" src="/media/T2-35.jpeg">
{{< /columns >}}

Then using the software in fabmodules we will select the machine type then set the home position in x & y directions 
(in my case x0=90 and y0 = 10) and we will adjust the Z manually using the Z up and down buttons 
on the machine, get the bit as close to the bed as you can then unscrew the collet to let the bit 
fall to be sure that it touches the bed then screw it again.

{{< columns >}}
<img Style="left;" src="/media/T2-36.jpeg">
<--->
<img Style="right;" src="/media/T2-25.png">
{{< /columns >}}

Now we are ready to set the other parameters on the fabmodules and start calculating,
 
Most important parameters are:
1. machine (we selected MDX-20).
2. speed (mm/s): we do 4 for tracing and 1 for cutting.
3. cut depth: should be 0.1 but we make it .15 to be sure it removes all the copper.
4. tool diameter which is 0.4 and I made it 0.5 as the clearance is wide in our design so I gave 
myself more space to solder.
5. number of offsets: if we set this option to -1 it will remove all the black area, but if we 
enter an intger number then we will determine how many paths it will create with the overlap 
determined in the offset overlap option, I put in 3 offsets.

After setting all the parameters we click on calculate then send to start the machine.

{{< columns >}}
<img Style="left;" src="/media/T2-39.jpeg">
<--->
<img Style="right;" src="/media/T2-26.png">
{{< /columns >}}

{{< hint info >}}
**Hint**\
If any problem happened you can cancel from the window that pops up and calculate the time in your
PC then click on move to xy0 zjog button then on the machine press the view button then clear the 
buffer by pressing both up and down buttons together the view led will start flashing once it's 
done you can press the view button again and repeat the above process.
{{< /hint >}}

After finishing the tracing process we can go to drilling and cutting the outline, we will do the
same select image for the drills and outline but in process we will choose PCB outline (1/32),
and remove the V-bit and fix the milling bit and adject the Z level.

In cutting process we will change the parameters to match the cutting, so the speed will set to 1
and cut depth will be 1.85 and tool diameter and we will notice that it changed the number of 
offsets to 1 automatically.

{{< columns >}}
<img Style="left;" src="/media/T2-38.jpg">
<--->
<img Style="right;" src="/media/T2-37.jpeg">
{{< /columns >}}


{{< columns >}}
<img Style="left;" src="/media/T2-28.png">
<--->
<img Style="right;" src="/media/T2-40.jpeg">
{{< /columns >}}

Remove the board after finishing the cutting process and clean it if there's any chip still on it.

<img Style="right;" src="/media/T2-42.jpeg">

Perfect, Now we can go to soldering.