---
title: 1- Prepare PCB desing for manufacturing
weight: -30
---

**What is PCB manufacturing process?** It's the construction of your board design, when you get
your board layout design on a real PCB and soldering the component, there's many ways to manufacture
PCBs, in this task we will go through manufacturing PCB using milling machine. Modela MDX-20

modela picture here

**First Step** is to Export gerber files from your design using the PCB design software 
which is in our case KiCad.

Open your board layout then from files choose "plot" option then select the layers you want to export,
in our case we only need the front layer, outline and the drills, then click on "Plot", drill files are 
exported seperatly from the down right button which will show you the screen on the right then click
"generate drill files".

{{< columns >}}
<img Style="left;" src="/media/T2-1.PNG">
<--->
<img Style="right;" src="/media/T2-2.PNG">
{{< /columns >}}

After exporting gerber files we will open them using gerbv software, then we will export each layer 
seperatly to **PDF** (exporting to pdf is to save the resolution and avoid dimension change).
So we will export the front layer with the outline and the drills also with the outline.

{{< hint info >}}
**Hint**\
you should export the traces with the outline even that we don't need the outline 
**to avoid dimension change**
{{< /hint >}}

{{< columns >}}
<img Style="left;" src="/media/T2-4.png">
<--->
<img Style="right;" src="/media/T2-5.png">
{{< /columns >}}

Now we have the PDF files for each layer, we need to turn them to a Black & white photo, Blade will
be the area we want to remove and white will be the traces.

We will use GIMP software to do that, open the PDF from files >> open then Click import but first
be sure what resolution you have typed the higher resolution the better quality so 1200 was fine 
for me.

{{< columns >}}
<img Style="left;" src="/media/T2-6.png">
<--->
<img Style="right;" src="/media/T2-7.PNG">
{{< /columns >}}

Then we will go to colors >> Shadow-Highlights then we will move the Hightligh bar to the left
to make is 0 value

{{< columns >}}
<img Style="left;" src="/media/T2-9.png">
<--->
<img Style="right;" src="/media/T2-10.png">
{{< /columns >}}

Then we will go to Image >> Mode >> indexed and check the "Use black and white (1-bit) palette" 
then Convert. after that we will go to Colors >> Invert.

{{< columns >}}
<img Style="left;" src="/media/T2-13.PNG">
<--->
<img Style="right;" src="/media/T2-14.png">
{{< /columns >}}

Our board should look like the left photo, Then we will Export As and type the name and 
the extension ".png"
We can try another way which is select traces and color them black then invert but I enjoyed
more doing it this way, Thanks to Alaa who introduced this way to me.

{{< hint info >}}
**Hint**\
We can color the outline black if we want to save time in tracing process, or we can leave it as 
indicator that the machine didn't shift when we change from tracing to cutting process.
{{< /hint >}}

{{< columns >}}
<img Style="left;" src="/media/T2-16.png">
<--->
<img Style="right;" src="/media/T2-17.PNG">
{{< /columns >}}

Let's do the same for the drill files but here we will color everything outside the outline black
to make it easier to detect while processing it on the machine.

<img src="/media/T2-18.png">

Hooray! we have the files and ready to go to the machine.