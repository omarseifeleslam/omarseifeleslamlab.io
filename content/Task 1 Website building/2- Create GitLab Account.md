---
title: 2- Create GitLab Account  
weight: -30
---

Hello, let's start with creating a gitlab account by going to [GitLab](gitlab.com) website.

1. choose to login from the top right corner of the screen, or wherever you find it as they might change interface.

<img src="/media/gitlab1.png"/>

2. Click on small word "Register now" under sign in button.

<img src="/media/gitlab2.png"/>

3. Add your information and a valid email address as they will send a confirmation mail which you should open, 
it will ask you to create a group project do it without hesitating whatever you do in this step you 
can edit later or even delete it just like I did.

<img src="/media/gitlab4.png"/>

Now you have a GitLab account, Congratulations.





























