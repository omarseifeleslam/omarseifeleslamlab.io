---
title: 1- What is git & gitlab
weight: -30
---

When we search about git, gitlab, github and similar keywords we will always see the word (Version Control), 
so I asked myself, then asked google to get a real answer, **What is Version Control ?** 
Version control is a system that records changes to a file or set of files over time so that you 
can recall specific versions later.

For example, If you are a graphic or web designer and want to keep every version of an image or layout 
(which you would most certainly want to), a Version Control System (VCS) is a very wise thing to use. 
It allows you to revert selected files back to a previous state, revert the entire project back to a previous 
state, compare changes over time, see who last modified something that might be causing a problem, 
who introduced an issue and when, and more. Using a VCS also generally means that if you screw things up or 
lose files, you can easily recover.

To read more about Version Control you can go to this [link](https://git-scm.com/book/en/v2/Getting-Started-About-Version-Control).

### So What is Git ?
Git is software for tracking changes in any set of files, usually used for coordinating work among 
programmers collaboratively developing source code during software development. Its goals include speed, 
data integrity, and support for distributed, non-linear workflows.

Git has three main states that your files can reside in: **modified**, **staged**, and **committed**

- Modified means that you have changed the file but have not committed it to your database yet.

- Staged means that you have marked a modified file in its current version to go into your next commit snapshot.

- Committed means that the data is safely stored in your local database.

This leads us to the three main sections of a Git project: **the working tree**, **the staging area**, 
and **the Git directory**.

<img src="/media/git1.png" alt="Working tree, staging area, and Git directory"/>

**The working tree** is a single checkout of one version of the project. These files are pulled out of the 
compressed database in the Git directory and placed on disk for you to use or modify.

**The staging area** is a file, generally contained in your Git directory, that stores information about what 
will go into your next commit. Its technical name in Git parlance is the “index”, but the phrase “staging area” 
works just as well.

**The Git directory** is where Git stores the metadata and object database for your project. This is the most 
important part of Git, and it is what is copied when you clone a repository from another computer.

To read more about git check this [book](https://git-scm.com/book/en/v2) which is very usefull.

### Then What is GitLab ?

A cloud service for remote hosting of git repositories (A lot like GitHub). In addition to hosting your code, 
the site helps manage software development projects with features like issue tracking, collaborating with other 
GitLab users, and hosting web pages.

GitLab offers free services for open source projects (accessible to the public) and paid tiers for private 
projects. For public projects, anyone can see code you push to GitLab and offer suggestions, or even code, 
to improve your project.