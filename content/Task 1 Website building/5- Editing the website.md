---
title: 5- Editing Content & Challenges
weight: -30
---
- As mention in the previous page, using Notepad and markdownViewer will ease the editing process alot.

- I will write down the challenges/errors I faced and how I got them solved and this will also show how we can edit 
a content of a page, add new page or remove one.

1. I wanted to add a new page named about me so I figured that each page to be added you should create a folder
and have the index file in it which indicates the page title.

<img src="/media/gitlab17.png">

2. Then you will create a file for each sub page you want to add inside it, my first challenge
was that I want to resize the image, then I wanted to make it inline with text then I want to add 
space between image and text, Thanks to Eng. Ahmed Saeed for the keywords (HTML, markdown & Padding)
which helped me alot in finding the HTML code that I used to do what I wanted.

`<img style="float: left; padding-right:10px" src="/media/omarseif.jpeg" alt="Omar Seif <" width="200"/>` 

3. Another error happened when I wanted to remove the example page so I just selected all and 
deleted them which was wrong because I figured that the "Posts" folder shouldn't be removed 
which includes the more section and the end of the index (probably the theme producer did that to save 
the copy rights).

<img src="/media/gitlab18.png">

4. one more challenge when I wanted to add 2 images beside each other and make the text above 
and under them and not inline with them so I edited the same code above to match my case and edited
the width to be suitable with page width.

```
<img Style="left;" src="/media/gitlab10.png" width="400">
<img Style="right;" src="/media/gitlab11.png" width="400">

```

{{< hint ok >}}
**Refreshing website**\
 While editing & pushing you may need to wait for about 2 mins for the website to get updated, 
 use ctrl + F5 and the website will get updated immediately. 
{{< /hint >}}