---
title: 3- Fork Example & Cloning to computer 
weight: -30
---
1. Once you are done with creating your GitLab account let's go with Forking the 
[example project](https://gitlab.com/ahmad._.saeed/ahmad._.saeed.gitlab.io) 
(to fork a project means to add someone's public project to your GitLab account projects).

<img src="/media/gitlab5.png">

2. Now you have the project at your GitLab account, We need to remove the fork relationship 
so we can edit it freely, and we will do that by clicking on the project then go to Settings -> General -> Advanced and
Remove fork relationship.

<img src="/media/gitlab6.png">

3. Now we can edit, but how, as we mentioned before that GitLab is a cloud service for remote 
hosting of git repositories, So first we need to install a tool for Git in our computer to start 
editing the project and the tool we will use is [Sourcetree](https://www.sourcetreeapp.com/).

4. After installing the Git gui tool we will need to make a relationship between our local repositories and GitLab
and this is Cloning the project, to do that you will need to Create SSH Public & Private keys from sourcetree app
tools-> Create or import SSH keys then generate and keep moving your curser inside the blank area until a code 
is generated.

<img Style="left;" src="/media/gitlab7.png" width="400">
<img Style="right;" src="/media/gitlab8.png" width="400">

5. Save both the public and private keys, then copy the public key from the box and go to GitLab account 
preferences (from the top right corner) then SSH (from the list on the left side of the page) then paste 
and save the public key and press the add key button.

<img src="/media/gitlab9.png">

6. Now click the arrow beside the icons in your taskbar you will find an Icon for "pageant" agent 
double click on it and choose to add a key and add the private key we saved in the previous step.

{{< hint info >}}
**Hint**\
You will need to add the private key to the pageant agent everytime you shutdown and open your computer.
{{< /hint >}}


<img Style="left;" src="/media/gitlab10.png" width="400">
<img Style="right;" src="/media/gitlab11.png" width="400">

7. Now click on the Clone button in Sourcetree app it will ask you to paste the URL of the project you want 
to clone, get that URL from Clone button in your GitLab project page and cope the "Clone with SSH" URL and paste
it to sourcetree.

<img src="/media/gitlab12.png">

- Now everything is set and we will publish our website.