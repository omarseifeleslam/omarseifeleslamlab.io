---
title: 4- Publishing & editing the website
weight: -30
---
1. In the gitlab account, go to your project's Settings -> General -> Advanced and
Change Path to [username].gitlab.io

<img src="/media/gitlab13.png">

2. Open the project folder on your PC and open config.yaml in 
[Notepad++ v7.5.6](https://notepad-plus-plus.org/downloads/v7.5.6/) and Change 
baseURL: https://ahmad._.saeed.gitlab.io/ to https://[username].gitlab.io/
- Download this plugin to use with notepad [MarkdownViewer](https://github.com/nea/MarkdownViewerPlusPlus/releases).

<img src="/media/gitlab14.png">

3. Now go to your project in sourcetree and from the left side menu click on File status then Stage all, 
write a comment to describe the job for your reference later then Commit, and Push. 

<img src="/media/gitlab15.png">

4. When you do that your website should be published but you will get pipeline error, there's one more new step, 
you will be asked to validate your account with a valid card and they will charge 1USD for confirmation, 
when you do this step wait a while and push again and everything will be working proberly and you will find 
a check mark beside your commit.

<img src="/media/gitlab16.png">

5. Congratulation the website is online now. you can check it from  https://omarseifeleslam.gitlab.io/

